import java.io.Serializable;
import java.lang.String;

/**
 * Created by AleM on 10/03/15.
 */
public class Car implements Serializable {
    private String model;
    private String colour;
    private int hp;
    private int doors;

    public Car(String model, String colour, int hp, int doors) {
        this.model = model;
        this.colour = colour;
        this.hp = hp;
        this.doors = doors;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }
}
