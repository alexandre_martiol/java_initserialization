import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by AleM on 10/03/15.
 */
public class Main {
    private static BufferedReader keyboard = new BufferedReader (new InputStreamReader (System.in));
    private static int option;
    private static ArrayList<Car> carsArray = new ArrayList<Car>();

    //Este metodo muestra el menu siempre que sea necesario
    private static int showMenu() throws IOException {
        System.out.println ("");
        System.out.println ("Menu:");
        System.out.println ("0. Exit:");
        System.out.println ("1. Add new Car:");
        System.out.println ("2. See all saved cars:");
        System.out.println ("3. Save list of cars in the system:");
        System.out.println ("4. Load list of cars:");
        System.out.println ("");

        int option = Integer.parseInt(keyboard.readLine());

        return option;
    }

    private static void newCar() throws IOException{
        String model, colour;
        int hp, doors;

        System.out.println ("");
        System.out.println ("What is the model of the car?");
        model = keyboard.readLine();
        System.out.println ("What is the colour of the car?");
        colour = keyboard.readLine();
        System.out.println ("How many HP has the car?");
        hp = Integer.parseInt(keyboard.readLine());
        System.out.println ("How many doors has the car?");
        doors = Integer.parseInt(keyboard.readLine());

        Car newCar = new Car(model,colour,hp,doors);

        carsArray.add(newCar);
    }

    private static void showCars() {
        System.out.println ("");
        System.out.println ("LIST OF SAVED CARS IN THE SYSTEM");

        if (carsArray.size() == 0) {
            System.out.println ("The list of cars is empty!");
        }

        else {
            for (int i = 0; i < carsArray.size(); i++) {
                Car c = carsArray.get(i);

                System.out.println("");
                System.out.println("Car number " + (i + 1) + ":");
                System.out.println("Model: " + c.getModel());
                System.out.println("Colour: " + c.getColour());
                System.out.println("HP: " + c.getHp());
                System.out.println("Doors: " + c.getDoors());
            }

            System.out.println("-------------------------------");
            System.out.println("");
        }
    }

    private static void saveCarsList() {
        if (carsArray.size() > 0) {
            try {
                FileOutputStream f = new FileOutputStream("carsList.ser");
                ObjectOutputStream s = new ObjectOutputStream(f);
                s.writeObject(carsArray);
                f.close();

                System.out.println("The list of cars has been saved!");
            } catch (IOException e) {
                e.printStackTrace();

                System.out.println("There was a problem...");
            }
        }

        else {
            System.out.println("There is nothing to save...");
        }
    }

    private static void loadCarsList() {
        try {
            FileInputStream f = new FileInputStream("carsList.ser");
            java.io.ObjectInputStream s = new ObjectInputStream(f);
            ArrayList<Car> array = (ArrayList<Car>) s.readObject();
            f.close();

            //Este bucle sirve para no perder los objetos que hemos añadido antes de cargar el fichero.
            for (int i = 0; i < array.size(); i++) {
                Car c = array.get(i);
                carsArray.add(c);
            }

            System.out.println ("The list of cars has been loaded!");
        }

        catch (Exception e) {
            e.printStackTrace();

            System.out.println ("There was a problem...");
        }
    }

    public static void main(String[] args) throws IOException, ParseException {
        option = showMenu();

        while (true) {
            switch (option) {

                //Salir
                case 0:
                    System.out.println("Goody Bye!");
                    System.exit(0);
                    break;

                case 1:
                    newCar();

                    option = showMenu();
                    break;

                case 2:
                    showCars();

                    option = showMenu();
                    break;

                case 3:
                    saveCarsList();

                    option = showMenu();
                    break;

                case 4:
                    loadCarsList();

                    option = showMenu();
                    break;

                //Control de opcion no valida
                default:
                    System.out.println("Wrong Option!");
                    option = showMenu();
                    break;
            }
        }
    }
}
